## Yato

A small repository where I'm gatherting useful snippets and abstractions for C++ development

If you find any bug, please feel free to submit an issue! It will be very helpful for me

## The project is moved to [GitHub](https://github.com/agruzdev/Yato)